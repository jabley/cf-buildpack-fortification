#!/usr/bin/env bats

@test "ruby hello world :: 401 response with no creds" {
  run curl -so/dev/null --write-out "%{http_code}" ${APP_ROUTE}/
  [ "${output}" = "401" ]
}

@test "ruby hello world :: 401 response with wrong creds" {
  run curl -so/dev/null --user wrong:password --write-out "%{http_code}" ${APP_ROUTE}/
  [ "${output}" = "401" ]
}

@test "ruby hello world :: 200 response with correct creds" {
  run curl -so/dev/null --user user1:password1 --write-out "%{http_code}" ${APP_ROUTE}/
  [ "${output}" = "200" ]
}

@test "ruby hello world :: HelloWorld response body with correct creds" {
  run curl -s --user user1:password1 ${APP_ROUTE}/
  echo "${output}" | grep -q "Hello, World!"
}

@test "ruby hello world :: /crash returns 502 and the crash appears in the event log" {
  until curl --fail -so/dev/null --user user1:password1 ${APP_ROUTE} >/dev/null 2>/dev/null; do sleep 0.5; done 
  run curl -so/dev/null --user user1:password1 -X POST -d foo=bar --write-out "%{http_code}" ${APP_ROUTE}/crash
  [ "${output}" = "502" ]
  run cf events ${APP_NAME}
  echo ${output} | grep -q app\.crash
}

@test "ruby hello world :: /kill-haproxy returns 502 and the crash appears in the event log" {
  until curl --fail -so/dev/null --user user1:password1 ${APP_ROUTE} >/dev/null 2>/dev/null; do sleep 0.5; done 
  run curl -so/dev/null --user user1:password1 -X POST -d foo=bar --write-out "%{http_code}" ${APP_ROUTE}/kill-haproxy
  [ "${output}" = "502" ]
  run cf events ${APP_NAME}
  echo ${output} | grep -q app\.crash
}

@test "ruby hello world :: /exit returns 5XX and the crash appears in the event log" {
  until curl --fail -so/dev/null --user user1:password1 ${APP_ROUTE} >/dev/null 2>/dev/null; do sleep 0.5; done 
  run curl -so/dev/null --user user1:password1 -X POST -d foo=bar --write-out "%{http_code}" ${APP_ROUTE}/exit
  [ ${output} -ge 500 ]
  run cf events ${APP_NAME}
  echo ${output} | grep -q app\.crash
}

@test "ruby hello world :: 3 crashes visible in the event log" {
  [ $(cf events ${APP_NAME} | grep -c app\.crash) -ge 3 ]
}

load helpers/cf-app-tools
load helpers/bats-tools

beforeAllDoAndPrintEvalableString() {
  [ -s tmp/f11n-buildpack-WIP.tgz ]
  setup_cf_app \
    "ruby-hw" \
    tests/fixtures/manifests/ruby-hw.yml \
    https://github.com/DigitalInnovation/ruby-hello-world \
    tests/fixtures/buildpacks/basic-auth.ruby:.buildpacks \
    tmp/f11n-buildpack-WIP.tgz
}

afterAll() {
  cleanup_cf_app
}
