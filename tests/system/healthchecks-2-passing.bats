#!/usr/bin/env bats

@test "healthchecks :: 2 configured, both passing :: /_proxy/client-check-1: 200 response" {
  run curl -so/dev/null --write-out "%{http_code}" ${APP_ROUTE}/_proxy/client-check-1
  [ "${output}" = "200" ]
}
@test "healthchecks :: 2 configured, both passing :: /_proxy/client-check-2: 200 response" {
  run curl -so/dev/null --write-out "%{http_code}" ${APP_ROUTE}/_proxy/client-check-2
  [ "${output}" = "200" ]
}

load helpers/cf-app-tools
load helpers/bats-tools

beforeAllDoAndPrintEvalableString() {
  [ -s tmp/f11n-buildpack-WIP.tgz ]
  setup_cf_app \
    "healthchecks-2" \
    tests/fixtures/manifests/healthchecks-2-passing.yml \
    https://github.com/jpluscplusm/staticfile-empty-repo \
    tests/fixtures/buildpacks/healthchecks.staticfile:.buildpacks \
    tests/fixtures/not-empty-file.txt:webroot/ \
    tmp/f11n-buildpack-WIP.tgz
}

afterAll() {
  cleanup_cf_app
}
