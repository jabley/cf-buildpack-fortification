#!/usr/bin/env bats
load helpers/bats-tools

beforeAllDoAndPrintEvalableString() {
  cat bin/compile
}

@test "bin/compile::yaml2json" {
  [ "$(echo '--- {}' | yaml2json)" = "{}" ]
}

@test "bin/compile::list_active_services:application" {
  list_active_services | grep -q application
}
@test "bin/compile::list_active_services:request-proxy" {
  list_active_services | grep -q request-proxy
}
@test "bin/compile::list_active_services:stats.cpu" {
  list_active_services | grep -q stats.cpu
}
@test "bin/compile::list_active_services:stats.memory" {
  list_active_services | grep -q stats.memory
}
@test "bin/compile::list_active_services:stats.network" {
  list_active_services | grep -q stats.network
}
@test "bin/compile::list_active_services:stats.haproxy" {
  list_active_services | grep -q stats.haproxy
}
@test "bin/compile::list_active_services:logs.haproxy" {
  list_active_services | grep -q logs.haproxy
}

@test "bin/compile::command_or_nothing:null vs null" {
  run command_or_nothing /dev/null /dev/null
  [ "${output}" = "" ]; [ $status -eq 0 ]
}

# Happy path
@test "bin/compile::command_or_nothing:Procfile vs null" {
  run command_or_nothing tests/fixtures/Procfile-command1 /dev/null
  [ "${output}" = "command1" ]; [ $status -eq 0 ]
}

@test "bin/compile::command_or_nothing:Procfile vs release" {
  run command_or_nothing tests/fixtures/Procfile-command1 tests/fixtures/release-command2
  [ "${output}" = "command1" ]; [ $status -eq 0 ]
}

@test "bin/compile::command_or_nothing:null vs release" {
  run command_or_nothing /dev/null tests/fixtures/release-command2
  [ "${output}" = "command2" ]; [ $status -eq 0 ]
}

# Unhappy path
@test "bin/compile::command_or_nothing:release vs Procfile" {
  run command_or_nothing tests/fixtures/release-command2 tests/fixtures/Procfile-command1
  [ "${output}" = "" ]; [ $status -eq 0 ]
}

@test "bin/compile::command_or_nothing:Procfile vs Procfile" {
  run command_or_nothing tests/fixtures/Procfile-command1 tests/fixtures/Procfile-command1
  [ "${output}" = "command1" ]; [ $status -eq 0 ]
}
  
@test "bin/compile::command_or_nothing:release vs release" {
  run command_or_nothing tests/fixtures/release-command2 tests/fixtures/release-command2
  [ "${output}" = "command2" ]; [ $status -eq 0 ]
}

@test "bin/compile::list_directories" {
  DOTCFDIR=abc BINDIR=def PROFILEDIR=ghi SCRIPTDIR=jkl APP_SVC_DIR=mno
  list_directories | grep -q abc
  list_directories | grep -q def
  list_directories | grep -q ghi
  list_directories | grep -q jkl
  list_directories | grep -q mno
}

@test "bin/compile::make_directories" {
  # Happy path, multiple directories, some already existing
  TEST_DIR=$(mktemp -d -t XXXXXXXX)
  run mkdir -p ${TEST_DIR}/def
  echo ${TEST_DIR}/{abc,def,ghi} | make_directories
  [ -d "${TEST_DIR}/abc" ]
  [ -d "${TEST_DIR}/def" ]
  [ -d "${TEST_DIR}/ghi" ]
  rm -rf ${TEST_DIR}

  # Unhappy path
  TEST_DIR=$(mktemp -d -t XXXXXXXX)
  echo ${TEST_DIR} | make_directories
  rm -rf ${TEST_DIR}

  echo | make_directories
}
