# The Application Fortification ("F11N") Buildpack

## Summary

This buildpack installs various metric reporting services into an application's
runtime.  It relies on a secondary buildpack, the Process Supervisor
(https://bitbucket.org/cf-utilities/cf-buildpack-process-supervisor) to
instantiate and regulate these services.

It also places a robust HTTP reverse proxy between the Cloud Foundry (CF)
router and the application. The proxy protects the application from overload by
limiting the number of concurrent requests which the application instance
receives, and provides extremely detailed logging and metrics for requests at
the HTTP protocol layer. This buildpack is not currently usable by TCP-routed
CF applications.

The proxy can also, once configured to do so (see "Configuration",
below), provide a degree of Origin Protection by only allowing authenticated
consumers to communicate with the application. This allows well-behaved
12-factor applications to be distributed across a set of CF installations,
having their requests load-balanced by a CDN which injects an authorisation
key. Thus the only route to the application is via the CDN, ensuring all
consumer requests satisify the CDN's application protection mechanism; there is
no direct "back-door" into any individual CF installation.

Finally, the buildpack normalises a set of diverse public Cloud Foundry
providers' runtime differences so that applications do not need to be aware
which CF it has been deployed on to. These differences include things such as
request protocol (TLS/HTTPS/HTTP), unique request IDs, and how the CF's
name/region is communicated to the application for logging, etc.

## Usage

From an application's `manifest.yml` or `cf push -b <buildpack>` invocation,
extract the current buildpack URL. Construct a `.buildpacks` file in the
application's repository's root directory which looks like this:

    <current buildpack URL>
    https://bitbucket.com/cf-utilties/cf-buildpack-fortification
    https://bitbucket.com/cf-utilties/cf-buildpack-process-supervisor

Next, modify the manifest or `cf push` invocation to reference the Multi
buildpack:

    https://bitbucket.com/cf-utilties/cf-buildpack-multi

NB At present the Multi buildpack only works with external buildpacks, not
those built in to the target Cloud Foundry. This limitation may be addressed in
the future.

Finally, `cf push` the application. During the staging process, you should
notice lines roughly matching these scroll by, after the application's usual
buildpack/runtime has finished:

    =====> Downloading Buildpack: https://bitbucket.org/cf-utilities/cf-buildpack-fortification
    =====> Detected Framework: Fortification
    =====> Downloading Buildpack: https://bitbucket.org/cf-utilities/cf-buildpack-process-supervisor
    =====> Detected Framework: Process Supervisor
    Using release configuration from last framework (Process Supervisor).

## Configuration

The following environment variables can be set by an application's
`manifest.yml` or `cf set-env` invocations in order to change the behaviour of
the buildpack during staging and its runtime.

Variables which represent times are specified in the format defined here,
including their unit suffix:
https://cbonte.github.io/haproxy-dconv/1.6/configuration.html#2.4

`FBP_RP_NAMESPACE_PREFIX`: A string containing the URL path prefix which the
proxy dedicates for its own use on inbound requests. Requests for URLs starting
with this string will never be received by the application.  **Default**:
`/_proxy`.

`FBP_RP_GLOBAL_MAXCONN`: A positive integer containing the FIXME read the HAP
manual **Default**: 5000.

`FBP_RP_INSTANCE_MAXCONN`: A positive integer containing the maximum number of
concurrent connections which will be accepted by the proxy before FIXME
read the goddam HAProxy manual. **Default**: 1000.

`FBP_RP_APP_MAXCONN`: A positive integer containing the maximum number of
concurrent connections the proxy will allow the app to receive, regardless of
the incoming request rate. NB The proxy does an exceptionally good job of
keeping the pipe beteen itself and application full; in other words, if there
is a significant number of requests to serve, then the application will see
this number of concurrent connections at all times. This *doesn't* represent "N
per second" or anything similar - this is the maximum simultaneous requests in
flight to the backend application. _It is strongly recommended you tune this
value before production deployment. The default is deliberately set too high so
you can observe the requests-in-flight inflection point at which a single
instance degrades under load, and can then specify an application-appropriate
value. See "Logging", below, for how to observe the maximum number of
requests-in-flight. **Default**: 1000.

`FBP_RP_DEFAULT_TIMEOUT_CONNECT`: a time variable detailing the maximum time
that the proxy will wait to connect the application before serving an HTTP 503.
It's recommended you leave this at its default. **Default**: 3s.

`FBP_RP_DEFAULT_TIMEOUT_CLIENT_INACTIVITY`: a time variable representing the
maximum time the proxy will wait for the client to send or acknowledge data
when it it's supposed to do so. **Default**: 30s.

`FBP_RP_DEFAULT_TIMEOUT_CLIENT_REQUEST`: a time variable representing the
maximum time the proxy will wait for a complete request header from the client.
This default is set quite low as the *proxy's* client is the Cloud Foundry
router, not the external user-agent. **Default**: 3s.

FBP_RP_DEFAULT_TIMEOUT_SERVER_INACTIVITY:-65s}
FBP_RP_DEFAULT_TIMEOUT_QUEUE:-30s}
FBP_RP_ACTIVE_USERNAMES:-""}

FBP_RP_USER_*

FBP_RP_HC_*

FBP_RP_DISABLE_ORIGIN_PROTECTION:false


# Information the F11N buildpack exposes to the application

## Envvars

CF_ZONE
CF_APP_NAME
CF_SPACE_NAME

## Request headers

TLS
CLIENT_IP

# Testing

