.PHONY: help test test-local test-prereq test-unit test-integration test-system tarball clean
.DEFAULT_GOAL = help
SHELL = /bin/bash

export CF_APP_PREFIX := f11n-test-by-$(USER)
export PATH := /tmp/bats-0.4.0/bin:/tmp/jq-1.3:$(PATH)
BATS = bats

test: test-prereq test-unit test-integration test-system clean ## Run all tests
	@echo "*** ALL TESTS PASSED ***"
test-local: test-prereq test-unit test-integration ## Run all non-network-dependent tests
	@echo "*** ALL LOCAL TESTS PASSED ***"

test-prereq: ## Run all prereq tests
	$(BATS) tests/prerequisites/*.bats
test-unit: ## Run all unit tests
	$(BATS) tests/unit/*.bats
test-integration: ## Run all integration tests
	$(BATS) tests/integration/*.bats
test-system: TEST_FILES := $(wildcard tests/system/*.bats)
test-system: tarball ## Run all system tests in parallel
	@$(MAKE) -j $(MAX_TESTS) $(TEST_FILES:bats=forcetest)

%.forcetest:
	$(BATS) $(@:forcetest=bats)

tarball:
	@mkdir -p tmp
	@rm -f tmp/f11n-buildpack-WIP.tgz
	@tar cfz tmp/f11n-buildpack-WIP.tgz --exclude tmp/ --exclude .git/ . 2>/dev/null

clean:
	-rm -f tmp/f11n-buildpack-WIP.tgz
	-cf apps | awk '/^$(CF_APP_PREFIX)/{print $$1}' | xargs -P5 -L1 -n1 cf delete -f -r

ci: BATS := bats --tap
ci: install-deps test-local

install-deps: install-bats install-jq

install-bats:
	@bats --version &>/dev/null || { curl --location -s https://github.com/sstephenson/bats/archive/v0.4.0.tar.gz | tar xfz - -C /tmp/; }
	@echo "$$(which bats): $$(bats --version)"
install-jq:
	@jq --version &>/dev/null || { mkdir -p /tmp/jq-1.3; curl --location -s https://www.jpluscplusm.com/jq-1.3-amd64 -o/tmp/jq-1.3/jq; chmod u+x /tmp/jq-1.3/jq; }
	@echo "$$(which jq): $$(jq --version)"

help: ## Show this help message
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
