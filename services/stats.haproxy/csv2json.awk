function quote(x) {
  if (x ~ /^[0-9]+$/) { return x }
  return ("\"" x "\"")
}
NR==1 {
  for (i = 1; i <= NF; i++) {
    header[i] = $i
  }
}
NR>1 {
  print "{"
  comma=0
  for (i = 1; i <= NF; i++) {
    if ($i!="") {
      print ((comma==1)?",":"") quote(header[i]) ":" quote($i)
    }
    comma=1
  }
  print "}"
}
